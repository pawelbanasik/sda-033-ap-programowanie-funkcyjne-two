package com.pawelbanasik;

public abstract class ArrayTask {
	private int[] data;

	public void setData(int[] data) {
		this.data = data;
	}

	protected abstract void doTask(int i);

	public void runTask() {
		for (int i : data) {
			doTask(i);
		}
	}
}
